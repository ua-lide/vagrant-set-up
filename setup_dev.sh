#!/bin/bash

# LIDE : Script de création des VMs et mise en place de tous les environnements en mode développement

help() {
cat << EOF
Usage : $0 [-h] [-x]
Script de création des VMs (virtualbox) et mise en place de tous les environnements en mode développement - LIDE
Options :
  -h  Affichage de ce message d'aide
  -x  Exécution. Par défaut - affichage de l'aide
EOF
}

OPTIND=1
EXECUTION=0

setup_web_front() {
  lidedir="~/LIDE"
  db_create="php bin/console doctrine:database:create"
  db_schema_create="php bin/console doctrine:schema:create"
  db_schema_update="php bin/console doctrine:schema:update --dump-sql"
  db_schema_force_update="php bin/console doctrine:schema:update --force"

  # chargement des données - Fixtures
  fixtures="php bin/console doctrine:fixtures:load"
  
  vagrant ssh -c "cd $lidedir; npm install; $db_create; $db_schema_create; $db_schema_update; $db_schema_force_update; $fixtures" web-front
}

#setup_web_back() {
  # TODO : Lancement apache et Serveur Web Socket
#}

while getopts hx opt; do
  case $opt in
    h)
      help
      exit 0
      ;;
    x)
      EXECUTION=1
      ;;
    *)
      help
      exit 1
      ;;
  esac
done
shift "$((OPTIND-1))" # Shift des options + optionnels

if [ $EXECUTION -eq 1 ]; then
  vagrant destroy
  vagrant up # Éventuellement commencer par faire un `vagrant destroy`
  setup_web_front

  # TODO Ici mettre le setup de web-back
else
  help
fi;
